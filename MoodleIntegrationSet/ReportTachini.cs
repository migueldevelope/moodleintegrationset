﻿using MoodleIntegrationSet.Views;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MoodleIntegrationSet
{
    public class ReportTachini
    {

        public void Report(string url, string token)
        {
            var client = new HttpClient
            {
                BaseAddress = new Uri(url)
            };

            client.DefaultRequestHeaders.Add("ContentType", "application/json");

            var coursespath = "/webservice/rest/server.php?wstoken=" + token + "&wsfunction=core_course_get_courses_by_field&moodlewsrestformat=json&field=category&value=5";
            var sql = "curso;inscrito";

            var lines = new string[1];
            lines.SetValue(sql, 0);
            System.IO.File.WriteAllLines("moodletachini.csv", lines);

            try
            {
                var result = client.GetAsync(url + coursespath).Result;
                var resultContent = result.Content.ReadAsStringAsync().Result;

                
                var listCourse = JsonConvert.DeserializeObject<ViewCourseByField>(resultContent);
                //Console.WriteLine("result: " + list);
                foreach (var item in listCourse.courses)
                {
                    Console.WriteLine("id user moodle:" + item.id);
                }

                Console.WriteLine("end");
            }
            catch (Exception)
            {

            }
        }

        
    }
}
