﻿using MoodleIntegrationSet.Views;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MoodleIntegrationSet
{
  public class MoodleIntegration
  {

    //public void importUsers(string url, string path)
    //{
    //  var client = new HttpClient
    //  {
    //    BaseAddress = new Uri(url)
    //  };

    //  client.DefaultRequestHeaders.Add("ContentType", "application/json");
    //  //clean view
    //  var sql = "IF EXISTS(SELECT TABLE_NAME";
    //  sql += " FROM INFORMATION_SCHEMA.VIEWS";
    //  sql += " WHERE  TABLE_NAME = N'JMVW_MOODLE_IMPORT_USER')";
    //  sql += " DROP VIEW JMVW_MOODLE_IMPORT_USER";
    //  sql += " GO";
    //  sql += " CREATE VIEW dbo.JMVW_MOODLE_IMPORT_USER";
    //  sql += " AS";

    //  var lines = new string[1];
    //  lines.SetValue(sql, 0);
    //  System.IO.File.WriteAllLines("viewmoodleuser.sql", lines);

    //  for (var i = 1001; i <= 3000; i++)
    //  {

    //    try
    //    {
    //      var result = client.GetAsync(url + path + i).Result;
    //      var resultContent = result.Content.ReadAsStringAsync().Result;

    //      var list = JsonConvert.DeserializeObject<List<ViewUser>>(resultContent);
    //      long row = 0;
    //      foreach (var item in list)
    //      {
    //        insertviewuser(i.ToString(), item.email, row);
    //        row += 1;
    //        System.Threading.Thread.Sleep(3000);
    //      }


    //      Console.WriteLine("id user moodle:" + i);
    //    }
    //    catch (Exception e)
    //    {

    //    }
    //  }


    //}

    public void import(string url, string pathView, string iduser)
    {
      var client = new HttpClient
      {
        BaseAddress = new Uri(url)
      };

      client.DefaultRequestHeaders.Add("ContentType", "application/json");
      //clean view
      /*var sql = "IF EXISTS(SELECT TABLE_NAME";
      sql += " FROM INFORMATION_SCHEMA.VIEWS";
      sql += " WHERE  TABLE_NAME = N'JMVW_MOODLE_IMPORT')";
      sql += " DROP VIEW JMVW_MOODLE_IMPORT";
      sql += "\n GO";
      sql += " \n CREATE VIEW dbo.JMVW_MOODLE_IMPORT AS";*/

      //var sql = "DELETE FROM JMTREMOO;";

      //var lines = new string[1];
      //lines.SetValue(sql, 0);
      //System.IO.File.WriteAllLines("viewmoodle.sql", lines);

      try
      {
        var result = client.GetAsync(url + pathView + iduser).Result;
        var resultContent = result.Content.ReadAsStringAsync().Result;
        
        var list = JsonConvert.DeserializeObject<List<ViewCourseUser>>(resultContent);
        //Console.WriteLine("result: " + list);
        foreach (var item in list)
        {
          //if (item.enablecompletation == "1")
          //{
          //Console.WriteLine("list: " + item.fullname);
          insertview(iduser, item.category, item.fullname.Replace("'", ""), item.startdate);
          //}
        }


        //Console.WriteLine("id user moodle:" + iuser);
      }
      catch (Exception)
      {

      }
    }



    public void importUser(string url, string path, string pathView)
    {
      var client = new HttpClient
      {
        BaseAddress = new Uri(url)
      };

      client.DefaultRequestHeaders.Add("ContentType", "application/json");
      //clean view
      /*var sql = "IF EXISTS(SELECT TABLE_NAME";
      sql += " FROM INFORMATION_SCHEMA.VIEWS";
      sql += " WHERE  TABLE_NAME = N'JMVW_MOODLE_IMPORT')";
      sql += " DROP VIEW JMVW_MOODLE_IMPORT";
      sql += "\n GO";
      sql += " \n CREATE VIEW dbo.JMVW_MOODLE_IMPORT AS";*/


      var begin = "BEGIN";
      var end = "END;";
      var sql = "DELETE FROM JMTREMOO;";


      var lines = new string[2];
      lines.SetValue(begin, 0);
      lines.SetValue(sql, 1);
      System.IO.File.WriteAllLines("0_viewmoodle.sql", lines);

      sql = "DELETE FROM JMTREMOOUSU;";

      lines = new string[2];
      lines.SetValue(begin, 0);
      lines.SetValue(sql, 1);
      System.IO.File.WriteAllLines("1_viewmoodleuser.sql", lines);

      sql = "BEGIN EXECUTE ANALISA_MOODLE; END;";

      lines = new string[1];
      lines.SetValue(sql, 0);
      System.IO.File.WriteAllLines("2_executeprocedure.sql", lines);



      try
      {
        Console.WriteLine("chamando api moodle");

        var result = client.GetAsync(url + path).Result;
        var resultContent = result.Content.ReadAsStringAsync().Result;

        var list = JsonConvert.DeserializeObject<ViewListUsers>(resultContent);
        foreach (var item in list.users)
        {
          string matricula = "0";
          try
          {
            if (item.customfields != null)
            {
              foreach (var custom in item.customfields)
              {
                if (custom.shortname == "matricula")
                {
                  if (custom.value.Contains("-"))
                    matricula = custom.value.Substring(0, custom.value.Length - 3);
                  else
                    matricula = custom.value;
                }
              }
            }

          }
          catch (Exception e)
          {

          }
          //if (matricula != "0")
          //{
            insertviewuser(item.id, matricula);
            import(url, pathView, item.id);
          //}

          var text = System.IO.File.ReadAllLines("1_viewmoodleuser.sql");
          lines = new string[text.Count() + 1];
          lines.SetValue(end, text.Count());
          System.IO.File.WriteAllLines("1_viewmoodleuser.sql", lines);


          var text2 = System.IO.File.ReadAllLines("0_viewmoodle.sql");
          lines = new string[text2.Count() + 1];
          lines.SetValue(end, text2.Count());
          System.IO.File.WriteAllLines("0_viewmoodle.sql", lines);


          Console.WriteLine("id user moodle:" + item.id);
        }

        Console.WriteLine("fim");
      }
      catch (Exception e)
      {

      }
    }



    public string insertviewuser(string user, string matricula)
    {
      try
      {
        var sql = "";

        sql = sql + "INSERT INTO JMTREMOOUSU VALUES(" + user + ", " + matricula + ");";

        string[] text = { sql };
        string[] lines = null;
        try
        {
          text = System.IO.File.ReadAllLines("1_viewmoodleuser.sql");
          lines = new string[text.Count() + 1];
          var count = 0;
          foreach (var item in text)
          {
            lines.SetValue(item, count);
            count += 1;
          }

          lines.SetValue(sql, text.Count());
        }
        catch (Exception)
        {
          lines = new string[1];
          lines.SetValue(sql, 0);
        }

        System.IO.File.WriteAllLines("1_viewmoodleuser.sql", lines);

        return "ok";
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public string insertview(string user, string category, string course, string date)
    {
      try
      {
        var sql = "";

        //sql = sql + "INSERT INTO JMTREMOO VALUES(" + user + ", '" + course + "', " + category + ", dbo.fn_ConvertToDateTime('" + date + "'));";
        sql = sql + "INSERT INTO JMTREMOO VALUES(" + user + ", '" + course + "', " + category + ", '" + date + "');";

        string[] text = { sql };
        string[] lines = null;
        try
        {
          text = System.IO.File.ReadAllLines("0_viewmoodle.sql");
          lines = new string[text.Count() + 1];
          var count = 0;
          foreach (var item in text)
          {
            lines.SetValue(item, count);
            count += 1;
          }

          lines.SetValue(sql, text.Count());
        }
        catch (Exception)
        {
          lines = new string[1];
          lines.SetValue(sql, 0);
        }

        System.IO.File.WriteAllLines("0_viewmoodle.sql", lines);

        return "ok";
      }
      catch (Exception e)
      {
        throw e;
      }
    }

  }
}
