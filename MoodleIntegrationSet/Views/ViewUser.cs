﻿using System.Collections.Generic;

namespace MoodleIntegrationSet.Views
{
  public class ViewUser
  {
    public string id { get; set; }
    public string email { get; set; }
    public List<ViewCustomFields> customfields { get; set; }
  }
}
