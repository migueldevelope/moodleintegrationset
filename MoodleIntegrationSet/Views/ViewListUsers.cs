﻿using System.Collections.Generic;

namespace MoodleIntegrationSet.Views
{
  public class ViewListUsers
  {
    public List<ViewUser> users { get; set; }
  }
}
