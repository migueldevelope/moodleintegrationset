﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoodleIntegrationSet.Views
{
  public class ViewCourseUser
  {
    public string id { get; set; }
    public string shortname { get; set; }
    public string fullname { get; set; }
    public string category { get; set; }
    public string startdate { get; set; }
    public string enddate { get; set; }
    public string enablecompletation { get; set; }
  }
}
