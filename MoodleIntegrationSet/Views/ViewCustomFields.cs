﻿
namespace MoodleIntegrationSet.Views
{
  public class ViewCustomFields
  {
    public string type { get; set; }
    public string value { get; set; }
    public string name { get; set; }
    public string shortname { get; set; }
  }
}
