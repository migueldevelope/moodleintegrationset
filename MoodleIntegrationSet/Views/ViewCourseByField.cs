﻿using System.Collections.Generic;

namespace MoodleIntegrationSet.Views
{
    public class ViewCourseByField
    {
        public List<ViewCourseByFieldList> courses { get; set; }
    }
}
